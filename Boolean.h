#pragma once
#include "type.h"
class Boolean : public Type
{
public:
	Boolean(bool boolean);
	std::string toString() const;
private:
	bool _boolean;
};