#pragma once
#include "Type.h"
class String : public Type
{
public:
	String(std::string string);
	std::string toString() const ;
private:
	std::string _string;
};