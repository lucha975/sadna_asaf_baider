#include "Integer.h"

Integer::Integer(int integer)
{
	this->_integer = integer;
}

std::string Integer::toString() const 
{
	return std::to_string(this->_integer);
}
