#pragma once
#include "Type.h"
class Integer : public Type
{
public:
	Integer(int Int);
	 virtual std::string toString() const override;
private:
	int _integer;
};