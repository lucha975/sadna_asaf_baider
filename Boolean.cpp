#include "Boolean.h"

Boolean::Boolean(bool boolean)
{
    this->_boolean = boolean;
}

std::string Boolean::toString() const
{
    return this->_boolean ? "true" : "false";
}
