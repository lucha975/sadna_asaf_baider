#include "Type.h"

Type::Type()
{
	this->_isTemp = false;
}

bool Type::getType() const
{
	return this->_isTemp;
}

void Type::setType(bool isTemp)
{
	this->_isTemp = isTemp;
}

bool Type::isPrintable()
{
	return true;
}

