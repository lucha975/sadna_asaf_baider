#include "String.h"

String::String(std::string string)
{
    this->_string = string;
}

std::string String::toString() const 
{
    return this->_string;
}
