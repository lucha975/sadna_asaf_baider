#include "Parser.h"
#include "IndentationException.h"
#include "SyntaxException.h"
#include <iostream>

Type* Parser::parseString(std::string str)
{
	if (str.find_first_not_of("\t ")|| str.length() <= 0)
	{
		try 
		{
			throw IndentationException();
		}
		catch (IndentationException& e)
		{
			std::cout << e.what() << std::endl;
		}
		return 0;

	}
	Helper::rtrim(str);
	Type* t = Parser::getType(str);
	if (t == nullptr)
	{
		try
		{
			throw SyntaxException();
		}
		catch (SyntaxException& e)
		{
			std::cout << e.what() << std::endl;
		}
	}
	else
	{
		if (t->isPrintable())
		{
			std::cout << t->toString();
		}

	}
	return t;
}

Type* Parser::getType(std::string str)
{
	Helper::trim(str);
	Type* n = nullptr;
	if (Helper::isInteger(str))
	{
		n = new Integer(std::stoi(str));
		n->setType(true);
		return n;
	}
	if (Helper::isBoolean(str))
	{
		n = new Boolean(str == "True" ? "True" : str == "False"? "False":"");
		n->setType(true);
		return n;
	}
	if (Helper::isString(str))
	{
		n = new String(str);
		n->setType(true);
		return n;
	}
	return nullptr;
}

bool Parser::isLegalVarName(std::string str)
{
	if (Helper::isDigit(str[0]))
	{
		return false;
	}
	int count = 0;
	int i = 0;
	bool underFound = false, letterFound = false, digitFound = false;
	for (auto c : str)
	{
		if (!underFound)
		{
			count += Helper::isUnderscore(str[i]) ? 1 : 0;
		}
		if (!letterFound)
		{
			count += Helper::isLetter(str[i]) ? 1 : 0;
		}
		if (!digitFound)
		{
			count += Helper::isDigit(str[i]) ? 1 : 0;
		}
		i++;
	}
}



