#pragma once
#include <string>

class Type
{
public:
	Type();
	bool getType() const;
	void setType(bool isTemp);
	virtual bool isPrintable();
	virtual std::string toString() const = 0 ;


private:
	bool _isTemp;
};
