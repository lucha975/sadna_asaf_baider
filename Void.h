#pragma once
#include "type.h"
class Void : public Type
{
public:
	bool isPrintable() override;
};